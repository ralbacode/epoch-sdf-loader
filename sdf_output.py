import logging

from .error_messager import ErrorMessager
from .sdf_block import SdfDataBlock
from .sdf_file import SdfFile

logging.basicConfig()

class SdfDataSet:
    """
    This class is used to hold all information pertaining to a particular wanted output.
    This includes the required data blocks from the sdf file, the file object itself
    allowing access to and from the file output, the file name in the form of a string
    and holds all retrieved blocks taken from the file.

    This class provides access to all stored variables and methods for setting and getting
    all of them.  It also provides an automated series of wrapper methods for getting all
    blocks.  This could be done manually, but I see no reason for wanting to leave it this
    way.

    IMPORTS:
    sdf.file.SdfFile as sfile
    sdf.dblock.SdfDataBlock as sblock

    ARGUMENTS:
        file_name: sdf_file
        *args: field names

    KEYWORD ARGUMENTS:
        messager_level:  build in messager logger
        input_dir:  input directory
        output_dir:  output directory
    """

    # Note for myself here.  This allows all arguments and kwargs to be accepted
    # This simplifies matters a lot since only accepted arguments are valid
    # I can set up a self documenting list of required parameters
    def __init__(self, file_name, *args,
               verbose=False,
               use_dependencies=True,
               input_dir=None,
               output_dir=None):
        # Set up error messager
        self.logger = logging.getLogger(__name__)

        if verbose:
            self.logger.setLevel(logging.DEBUG)
        self.fields_out = []
        self.output_blocks = {}
        for arg in args:
            self.fields_out.append(arg)

        self.input_dir = input_dir
        self.output_dir = output_dir
        # This needs simplification and refactoring with Pathlib
        self.logger.info(f"output directory set to {self.output_dir}")
        self.logger.info("input directory set to {self.input_dir}" + self.input_dir)
        self.file_name = file_name + ".sdf"
        self.logger.info("file name set to " + self.file_name)
        self.sdf_file = SdfFile(self.file_name, input_dir=self.input_dir,
                                verbose=verbose)
        self.logger.info("checking if SdfFile built")
        self.use_dependencies = use_dependencies

    def get_block(self, block_nm_s):
        """Using a reference string given a field or output value, attempts to locate and
        retrieve all information related to that variable or value from the file.
        """
        # Tries to output a block element from the output dictionary
        try:
            self.logger.info("Getting block : " + block_nm_s)
            return self.output_blocks[block_nm_s]
        # If it fails to find a key, it informs the user
        except KeyError:
            print("Invalid key given for output dictionary")

    def set_new_dependencies(self, block_checked):
        """ Upon calling, any new block dependencies are returned.  This generally takes a
        block as an operator and uses _get_block_dependencies within that block.
        """
        # This calls the get_block_dependencies method for the referenced block
        block_dep = block_checked.get_block_dependencies()

        # The block dependencies method then returns a tuple which is converted to string form using the
        # block register.  This can then be added to the output dictionary

        # Note that I need to improve kwargs for this system.
        if block_dep[0]:
            block_dep_set = (block_dep[1],)
            self.add_output_blocks(block_dep_set)

    def build_all_blocks(self):
        """Builds all blocks referenced in the output dictionary provided those outputs
        have absolutely no data.

        This runs through the output dictionary _output.od, initialises a new block
        using SdfDataBlock, then gets any new dependencies and adds them at the end of
        the output dictionary.

        It keeps doing this until all blocks have been set.
        """
        for block_in_use in self.fields_out:
            if block_in_use in self.output_blocks:
                self.logger.info("Block already present, skipping")
            else:
                self.output_blocks[block_in_use] = SdfDataBlock(block_in_use, self.sdf_file,
                                                                messager_level=self.logger_level)
                if self.use_dependencies:
                    self.set_new_dependencies(self.output_blocks[block_in_use])

    def print_blocks_in_sdf_file(self):
        print("Blocks in current sdf_file:  ", self.sdf_file.name)
        for key, val in self.sdf_file.block_register_d.items():
            print(key)

    def build_block(self, block_name):
        if block_name in self.sdf_file.block_register_d:
            if block_name in self.output_blocks:
                self.logger.warning("block already present")
            else:
                self.logger.info("building block")
                self.output_blocks[block_name] = SdfDataBlock(block_name, self.sdf_file, messager_level=self.logger_level,
                                                              auto_build=False)
                self.output_blocks[block_name].build_all_block_substructures()
                if self.use_dependencies:
                    self.set_new_dependencies(self.output_blocks[block_name])
                    self.build_all_blocks()

        else:
            self.logger.warning("no block found with that SDF name")

    def get_all_blocks(self):
        """Returns the entire stored output dictionary.  Generally only used for data
        dumping or for printing.

        Using get_block is a much better way of returning data for plotting use.
        """
        return self.output_blocks

    def add_output_blocks(self, output_blocks):
        """
        Using the input arguments given for the referenced tuple output_t, iterates
        through the values given and sets them as individual elements within an ordered
        dictionary.

        """
        for out_block in output_blocks:
            self.fields_out.append(out_block)
