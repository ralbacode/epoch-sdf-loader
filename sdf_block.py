import logging

from .error_messager import ErrorMessager
from .shared_methods import *

logging.basicConfig()

class SdfDataBlock:
    """
    This class provides all methods and data structures needed to handle the output of a particular
    data block from a given SDF file.

    ARGS:
    name - block name
    sdf_file - linked SdfFile object.  This is not a standard file handler, but instead an instance of SdfFile

    KWARGS:
    messager_level - sets level of built in error_messenger
    auto_build - automatically builds block.  Defaults to True, set to False if examining block structures

    """
    data_type_d = {
        1: np.int32,
        2: np.int64,
        3: np.float32,
        4: np.float64,
        5: np.dtype('f16'),
        6: np.uint8,
        7: np.bool_,
        8: "No value"
    }

    data_type_size_d = {
        1: 4,
        2: 8,
        3: 4,
        4: 8,
        5: 16,
        6: 1,
        7: 1,
        8: 0
    }

    block_type_meta_d = {
        -1: meta_ignore,
        0: meta_ignore,
        1: meta_plain_mesh,
        2: meta_point_mesh,
        3: meta_plain_var,
        4: meta_point_var,
        5: meta_ignore,
        6: meta_ignore,
        7: meta_ignore,
        8: meta_ignore,
        9: meta_ignore,
        10: meta_ignore,
        11: meta_ignore,
        12: meta_ignore,
        13: meta_ignore,
        20: meta_ignore
    }

    block_type_data_d = {
        -1: data_ignore,
        0: data_ignore,
        1: data_plain_mesh,
        2: data_point_mesh,
        3: data_plain_var,
        4: data_point_var,
        5: data_ignore,
        6: data_ignore,
        7: data_ignore,
        8: data_ignore,
        9: data_ignore,
        10: data_ignore,
        11: data_ignore,
        12: data_ignore,
        13: data_ignore,
        20: data_ignore
    }

    def __init__(self, name, sdf_file, verbose=False, auto_build=True):
        # Associate sdf file

        self.logger = logging.getLogger(__name__)
        if verbose:
            self.logger.setLevel(logging.DEBUG)

        self.name = name
        self.data_type_length = None
        self.data_length = None
        self.data_type = None
        self.num_dims = None
        self.block_title = None
        self.data = None
        self.vals = None

        # Mesh variables
        self.grid_points = None
        self.num_particles = None
        self.normalisation = None
        self.axis_labels = None
        self.axis_units = None
        self.min_value = None
        self.max_value = None
        self.geometry = None
        self.mesh_id = None
        self.auto_build = auto_build

        # A dependency flag to determine how the block operates
        # This tells any subsequence routine if this block can be used to display other blocks
        self.is_dependency = False

        # Autobuild the block using the three routines
        self.logger.info(f"Building block {self.name}")
        if self.auto_build:
            self.build_all_block_substructures()
        self.logger.info(f"{self.name} built")

    def build_metadata_only(self):
        """Build block metadata without retrieving data.  Useful for diagnostics"""
        self.build_block_header()
        self.build_block_metadata()

    def print_block_metadata(self):
        """Dumps the block meta_data to screen.  Useful for diagnosing potential problems in an SDF file"""
        print("===========================================")
        print("Block\t\t", self.name)
        print("Block title\t\t", self.block_title)
        print("Data length:\t\t", self.data_length)
        print("Data type:\t\t", self.data_type)
        print("Dimensions\t\t", self.num_dims)
        for key, val in self.vals.items():
            print(key, ":\t\t", val)
        print("===========================================")


    def get_block_dependencies(self):
        """Gets all block dependencies for the current block.  This can be used once the block header has been set as part of block initialisation.
        The block dependencies are only returned if there is a non-zero value for the block
        header.  This process is usually automatic"""
        self.logger.info("determining block dependency")
        if self.mesh_id is not None:
            self.logger.info("block dependency found:  "+self.mesh_id)
            return True, self.mesh_id
        else:
            self.is_dependency = True
            return False, 0

    def build_block_header(self):
        """Sets the block header.  Every block header is identical - only the meta data contained
        for each block type notably differs.  The header data is stored in block_info_d."""
        # Seek initial block location using built block register stored with file object
        self.logger.info("building block header")
        self.sdf_file.f_handle.seek(self.sdf_file.block_register_d[self.name])

        # Build as normal.  First record is a dummy to prevent unnecessary data doubling
        self.data_location = np.fromfile(self.sdf_file.f_handle, dtype=np.int64, count=1)[0]
        self.data_location = np.fromfile(self.sdf_file.f_handle, dtype=np.int64, count=1)[0]
        self.block_id = conv_uchar_str(
            np.fromfile(self.sdf_file.f_handle, dtype=np.uint8, count=32))
        self.data_len = np.fromfile(self.sdf_file.f_handle, dtype=np.int64, count=1)[0]
        self.block_type = np.fromfile(self.sdf_file.f_handle, dtype=np.int32, count=1)[0]
        self.data_type = np.fromfile(self.sdf_file.f_handle, dtype=np.int32, count=1)[0]
        self.data_type_length = self.data_type_size_d[self.data_type]
        self.data_length = int(self.data_len / self.data_type_length)
        self.data_type = self.data_type_d[self.data_type]
        self.num_dims = np.fromfile(self.sdf_file.f_handle, dtype=np.int32, count=1)[0]
        self.block_title = conv_uchar_str(
            np.fromfile(self.sdf_file.f_handle, dtype=np.uint8, count=32))
        self.logger.info("block header built")

    def build_block_metadata(self):
        """To be able to build the block metadata and data properly, it should be feasible to pass the
        SdfFile itself as a parameter to the resulting block data.  This gives access to all the native
        functions.
        """

        # Seek the block location by combining the blocks location and the header length
        self.sdf_file.f_handle.seek(self.sdf_file.block_register_d[self.name] +
                                    self.sdf_file.header_length)

        # Use referenced block type to determine which sub function to access using reference dictionary
        self.block_type_meta_d[self.block_type](self, self.sdf_file)

    def build_block_data(self):
        """Set the data for a given sdf output block.  This calls the set_block_data routine in
        SdfGeneralRoutines which then returns a numpy array related to the required type and
        data type.  This is a very simple abstract factory taking advantage of pythons
        dictionary routines."""

        # Find data location and locate the data itself
        self.sdf_file.f_handle.seek(self.data_location)
        self.block_type_data_d[self.block_type](self, self.sdf_file)
        self.logger.info("block data built")

    def build_all_block_substructures(self):
        """Builds all substructures for a given block"""
        self.build_block_header()
        self.build_block_metadata()
        self.build_block_data()
