import logging

from .sdf_file import SdfFile
from .sdf_output import SdfDataSet

logging.basicConfig()
logger = logging.getLogger()
logger.setLevel(logging.INFO)

def get_blocks_from_sdf(sdf, blocks, **data_set_kwargs):
    """Gets required blocks from the sdf file given.  Name of sdf file need only be given, the extension name need not be given."""
    try:
        if type(blocks) is 'str':
            data_out = SdfDataSet(sdf, blocks, **data_set_kwargs)
            data_out.build_all_blocks()
            return data_out.get_all_blocks()
        elif type(blocks) is not None:
            data_out = SdfDataSet(sdf, *blocks, **data_set_kwargs)
            data_out.build_all_blocks()
            return data_out.get_all_blocks()
        else:
            logger.info("No block given for use")
    except IOError:
        logger.info("SDF file path invalid, returning nothing")
        return None


def list_blocks_in_sdf(sdf, **file_kwargs):
    """Returns a logger.infoed list of all the blocks within a given file"""
    if ".sdf" not in sdf:
        sdf += ".sdf"
    logger.info(sdf)
    try:
        file = SdfFile(sdf, **file_kwargs)
        file.print_blocks_in_sdf_file()
    except IOError:
        logger.info("SDF file path invalid")
